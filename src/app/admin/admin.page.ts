import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventService } from '../event.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {
  data: any;
  user:any;
  AdminForm = new FormGroup({
    edits: new FormControl('', Validators.required),
    scan: new FormControl('', Validators.required),
    amount: new FormControl('',Validators.required)
  });
  constructor(private eveSer:EventService , private router:Router,private route: ActivatedRoute) { 
    this.AdminForm.value.edits=false;
    this.AdminForm.value.scan=false;
    this.AdminForm.value.amount=false;

  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.user = this.router.getCurrentNavigation().extras.state.user;
       }
    });
    console.log(this.user);
    console.log(this.AdminForm.value.edits);
    console.log(this.AdminForm.value.scan);
  }
  save(){
    this.eveSer.setAdmin("Edits",this.AdminForm.value);
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user,
        edits:this.AdminForm.value.edits,
        scan:this.AdminForm.value.scan,
        amount:this.AdminForm.value.amount,

      }
    };
    this.router.navigate(['event'], navigationExtras);
  }
}
