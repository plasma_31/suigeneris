import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'admin', loadChildren: './admin/admin.module#AdminPageModule' },
  { path: 'event', loadChildren: './event/event.module#EventPageModule' },
  { path: 'editform', loadChildren: './editform/editform.module#EditformPageModule' },
  { path: 'barcode', loadChildren: './barcode/barcode.module#BarcodePageModule' },
  { path: 'event-amount', loadChildren: './event-amount/event-amount.module#EventAmountPageModule' },
  { path: 'participants', loadChildren: './participants/participants.module#ParticipantsPageModule' },
  { path: 'registration', loadChildren: './registration/registration.module#RegistrationPageModule' },
  { path: 'scan', loadChildren: './scan/scan.module#ScanPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
