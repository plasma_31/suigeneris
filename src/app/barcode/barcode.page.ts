import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import { EventService } from '../event.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { SelectorListContext, HtmlParser } from '@angular/compiler';

@Component({
  selector: 'app-barcode',
  templateUrl: './barcode.page.html',
  styleUrls: ['./barcode.page.scss'],
})
export class BarcodePage implements OnInit {
  id = null;
  user: any;
  participants: any[];
  currparticipant: any;
  constructor(
    private eveSer: EventService,
    private route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.user = this.router.getCurrentNavigation().extras.state.user;
       }
    });
    if (this.user) {
    this.eveSer.getEvent(this.user.event).subscribe(success => {
      this.participants = success;
    }, error => {
      console.log(error);
    });
  }
  }
  Create() {
    let i;
    console.log(this.participants);
    if (this.participants) {
    for (i = 0 ; i < this.participants.length; i++) {
      if (
        this.participants[i].data.college === this.user.college &&
        this.participants[i].data.email === this.user.email &&
        this.participants[i].data.number === this.user.number) {
        this.id = this.participants[i].id;
        this.currparticipant=this.participants[i].data;
        this.toastCtrl.create({
          message: 'Screenshot and share the QRCode',
          duration: 2000,
          color: 'medium',
          position: 'bottom'
        }).then(toast => {
          toast.present();
        });
        }
      }
    }
  }
}
