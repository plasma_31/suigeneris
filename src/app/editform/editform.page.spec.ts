import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditformPage } from './editform.page';

describe('EditformPage', () => {
  let component: EditformPage;
  let fixture: ComponentFixture<EditformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditformPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
