import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventService } from '../event.service';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-editform',
  templateUrl: './editform.page.html',
  styleUrls: ['./editform.page.scss'],
})
export class EditformPage implements OnInit {
  eventForm = new FormGroup({
    event: new FormControl('', Validators.required),
    name: new FormControl('', Validators.required),
    number: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    college: new FormControl('', Validators.required),
    year: new FormControl('', Validators.required),
    department: new FormControl('', Validators.required),
    payment: new FormControl('', Validators.required),
    pending: new FormControl('')
  });
  participant: any;
  amount: any;
  val: number = 0;
  constructor(
    private eveSer: EventService,
    private route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.participant = this.router.getCurrentNavigation().extras.state.user;
       }
      this.eveSer.getAmount().subscribe(success => {
        this.amount = success;
        console.log(success);
         }, error => {
          console.log(error);
        });
      console.log(this.participant);
      if (this.participant) {
         this.eventForm.patchValue(this.participant.data);
       }
    });
  }
  submit() {
    let i;
    for (i = 0; i < this.amount.length; i++) {
      if (this.amount[i].id === this.eventForm.value.event) {
        if (this.eventForm.value.payment > this.amount[i].data.amount) {
          this.alertCtrl.create({

            header: 'Failed',
            message: 'Invalid Amount',
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel'
                }
            ]
        }).then(alertEl => {
            alertEl.present();
        });
          this.val = 1;
          break;
        } else {
        this.eventForm.value.pending = this.amount[i].data.amount - this.eventForm.value.payment;
        }
      }
    }
    if (this.participant && this.val === 0) {
      console.log(this.participant.id);
      console.log('Small Change');
      if (this.eventForm.value.event === "D'Bugging(IEEE)") {
      this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      }else if (this.eventForm.value.event === "D'Bugging(NON-IEEE)") {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      }else if (this.eventForm.value.event === 'Protovision') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Unravel') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'SID') {
      this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Matargashti') {
      this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Deadshot') {
      this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Pubg(DUO)') {
      this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      }else if (this.eventForm.value.event === 'Pubg(SQUAD)') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      }else if (this.eventForm.value.event === 'CSGO') {
      this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      }  else if (this.eventForm.value.event === 'VR') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      }else if (this.eventForm.value.event === 'Rugby') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Knock It Down') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Carrom') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Arm Wrestling') {
        this.eveSer.updateEvent(this.eventForm.value.event, this.participant.id, this.eventForm.value);
      }
      this.alertCtrl.create({
        header: 'Success',
        message: 'Form Submitted Succesfully',
        buttons: [
            {
                text: 'Ok',
                role: 'cancel'
            }
        ]
    }).then(alertEl => {
       alertEl.present();
    });
      if (this.eventForm.value.pending === 0) {
      const navigationExtras: NavigationExtras = {
        state: {
          user: this.eventForm.value
        }
      };
      this.router.navigate(['barcode'], navigationExtras);
    }
      this.participant = null;
      this.eventForm.reset();
    }
    this.val = 0;

  }
}
