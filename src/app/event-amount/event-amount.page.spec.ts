import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventAmountPage } from './event-amount.page';

describe('EventAmountPage', () => {
  let component: EventAmountPage;
  let fixture: ComponentFixture<EventAmountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventAmountPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAmountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
