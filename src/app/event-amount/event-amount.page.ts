import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {EventService} from '../event.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-event-amount',
  templateUrl: './event-amount.page.html',
  styleUrls: ['./event-amount.page.scss'],
})
export class EventAmountPage implements OnInit {
  amountForm = new FormGroup({
    event: new FormControl('', Validators.required),
    amount: new FormControl('', Validators.required),
  });
  constructor(private eveSer: EventService, private alertCtrl: AlertController) { }
  event: any;
  ngOnInit() {
    this.eveSer.getAmount().subscribe(success => {
      this.event = success;
      console.log(success);
    }, error => {
      console.log(error);
    });
  }
  Add() {
    this.eveSer.addAmount(this.amountForm.value.event, this.amountForm.value);
    this.alertCtrl.create({

    header: 'Success',
    message: 'Amount Added Succesfully',
    buttons: [
        {
            text: 'Ok',
            role: 'cancel'
        }
    ]
}).then(alertEl => {
    alertEl.present();
});
    this.amountForm.reset();
}

}
