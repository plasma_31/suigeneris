import { Component, OnInit, Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventService } from '../event.service';
import { AlertController, ToastController, IonDatetime } from '@ionic/angular';
import {Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { getLocaleDateTimeFormat } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: 'app-event',
  templateUrl: './event.page.html',
  styleUrls: ['./event.page.scss'],
})

export class EventPage implements OnInit {
  eventForm = new FormGroup({
    event: new FormControl('', Validators.required),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    number: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.pattern('.+\@.+\..+')]),
    college: new FormControl('', [Validators.required, Validators.minLength(3)]),
    year: new FormControl('', Validators.required),
    department: new FormControl('', Validators.required),
    payment: new FormControl('', Validators.required),
    registeredBy: new FormControl(''),
    pending: new FormControl(''),
    date:new FormControl('')
  });
  val: number = 0;
  pendingcheck:any;
  user: any;
  committee: any;
  admin: any;
  Alledits: any;
  editing: any;
  scanning: any;
  date: any;
  constructor(
    private eveSer: EventService,
    private route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController) {

    }
  users: any[

  ];
  amount: any[];
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.user = this.router.getCurrentNavigation().extras.state.user;
        this.Alledits = this.router.getCurrentNavigation().extras.state.edits;
       }
    });
    this.eveSer.getAdmin().subscribe(success => {
      this.Alledits = success;
      console.log(success);
      this.EditStatus();
       }, error => {
        console.log(error);
      });
    this.eveSer.getAmount().subscribe(success => {
      this.amount = success;
      console.log(success);
       }, error => {
        console.log(error);
      });
    this.eveSer.getallUsers().subscribe(success => {
      this.users = success;
    }, error => {
      console.log(error);
    });

    if (this.user) {
      this.committee = this.user.data.post;
      this.admin = this.user.data.name;
      console.log(this.user);
    }
  }
  regUsers() {
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.router.navigate(['participants'], navigationExtras);
  }
  addAmount() {
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.user
      }
    };
    this.router.navigate(['event-amount'], navigationExtras);
  }
  EditStatus() {
    let i;
    for ( i = 0; i < this.Alledits.length; i++) {
        this.editing = this.Alledits[i].data.edits;
        this.scanning = this.Alledits[i].data.scan;
        this.amount=this.Alledits[i].data.amount;
    }
    console.log(this.editing);
    console.log(this.scanning);
  }
  submit() {
      let i;
      for ( i = 0 ; i < this.amount.length; i++) {
          if (this.amount[i].id === this.eventForm.value.event) {
            if (this.eventForm.value.payment > this.amount[i].data.amount) {
              this.alertCtrl.create({

                header: 'Failed',
                message: 'Invalid Amount',
                buttons: [
                    {
                        text: 'Ok',
                        role: 'cancel'
                    }
                ]
            }).then(alertEl => {
                alertEl.present();
            });
              this.val = 1;
              break;
            } else {
            this.eventForm.value.pending = this.amount[i].data.amount - this.eventForm.value.payment;
            console.log(this.eventForm.value.pending);
              this.pendingcheck=this.eventForm.value.pending;
            }
          }
        }
      this.eventForm.controls['registeredBy'].setValue(this.user.data.name);
      this.date = new Date().toISOString();
      this.eventForm.controls['date'].setValue(this.date);
      this.eventForm.controls['pending'].setValue(this.pendingcheck);
      console.log(this.pendingcheck);
      if (this.val === 0) {
      if (this.eventForm.value.event === "D'Bugging(IEEE)") {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === "D'Bugging(NON-IEEE)") {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      }else if (this.eventForm.value.event === 'Protovision') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Unravel') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'SID') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Matargashti') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Deadshot') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Pubg(DUO)') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Pubg(SQUAD)') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'CSGO') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Rugby') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Knock It Down') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Carrom') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      } else if (this.eventForm.value.event === 'Arm Wrestling') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      }else if (this.eventForm.value.event === 'VR') {
        this.eveSer.addEvent(this.eventForm.value.event, this.eventForm.value);
      }
     // this.eveSer.add(this.eventForm.value);
      this.alertCtrl.create({

      header: 'Success',
      message: 'Form Submitted Succesfully',
      buttons: [
          {
              text: 'Ok',
              role: 'cancel'
          }
      ]
  }).then(alertEl => {
      alertEl.present();
  });
  console.log(this.eventForm.value.pending);
  if (this.pendingcheck === 0) {
    const navigationExtras: NavigationExtras = {
      state: {
        user: this.eventForm.value
      }
    };
    this.router.navigate(['barcode'], navigationExtras);
  }
    this.eventForm.reset();
  }
  this.val = 0;

}
}
