import { Component, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router,NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';
import {EventService} from '../event.service';

@Component({
  selector: 'app-home',
  encapsulation: ViewEncapsulation.None,
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  loginForm = new FormGroup({
    uname: new FormControl('', Validators.required),
    upass: new FormControl('', Validators.required)
  });
  users: any[] = [
  ];

  constructor(
      private router: Router,
      private alertCtrl: AlertController,
      private eveSer: EventService,
      ) {}
  ngOnInit() {
    this.eveSer.getallUsers().subscribe(success => {
      console.log(success);
      this.users = success;
    }, error => {
      console.log(error);
    });
  }
  Login() {
    let i;
    let check;
    for (i = 0; i < this.users.length; i++) {
        if ((this.loginForm.value.uname) === "Nihar" && (this.loginForm.value.uname) === this.users[i].data.name &&(this.loginForm.value.upass) === this.users[i].data.password) {
          check = 'success';
          let navigationExtras: NavigationExtras = {
            state: {
              user: this.users[i]
            }
          };
          this.router.navigate(['admin'], navigationExtras);
          this.loginForm.reset();
          break;
        }else {
          if ((this.loginForm.value.uname) === this.users[i].data.name && (this.loginForm.value.upass) === this.users[i].data.password) {
            check = 'success';
            let navigationExtras: NavigationExtras = {
              state: {
                user: this.users[i]
              }
            };
            this.router.navigate(['event'], navigationExtras);
            this.loginForm.reset();
            break;
            }else {
              check = null;
            }
        }
      }
      if (check != null) {
        console.log('Login Success');
        //this.router.navigateByUrl('event');
      } else {
          this.alertCtrl.create({
            header: 'Invalid Login',
            message: 'Invalid Username or Password',
            buttons: [
                {
                    text: 'Ok',
                    role: 'cancel'
                }
            ]
          }).then(alertEl => {
            alertEl.present();
          });
        }
    
}
}
