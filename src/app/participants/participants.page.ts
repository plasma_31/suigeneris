import { Component, OnInit } from '@angular/core';
import {EventService} from '../event.service';
import { AlertController } from '@ionic/angular';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { EventPage } from '../event/event.page';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.page.html',
  styleUrls: ['./participants.page.scss'],
})
export class ParticipantsPage implements OnInit {
  data: any;
  PartForm = new FormGroup({
    event: new FormControl('', Validators.required)
  });
  Alledits:any;
  editing: any;
  scanning: any;
  constructor(
    private eveSer: EventService,
    private evePage: EventPage,
    private route: ActivatedRoute,
    private router: Router,
    private alertCtrl: AlertController) {

  }
  participants: any[];
  amount: any[];
  user: any;
  participant: any;
  pending: any[];
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.user = this.router.getCurrentNavigation().extras.state.user;
      }
    });
    this.eveSer.getAdmin().subscribe(success => {
      this.Alledits = success;
      console.log(success);
      this.EditStatus();
       }, error => {
        console.log(error);
      });
    if (this.participant) {
      console.log(this.participant);
    }
    if (this.user) {
      console.log(this.user);
    }
    this.eveSer.getAll().subscribe(success => {
      this.participants = success;
      console.log(success);
       }, error => {
        console.log(error);
      });

  }
  EditStatus() {
    let i;
    for ( i = 0; i < this.Alledits.length; i++) {
        this.editing = this.Alledits[i].data.edits;
        this.scanning = this.Alledits[i].data.scan;
    }
    console.log(this.editing);
    console.log(this.scanning);
  }
  getTable() {
    if (this.PartForm.value.event === "D'Bugging(IEEE)") {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
         }, error => {
          console.log(error);
        });
     }else if (this.PartForm.value.event === "D'Bugging(NON-IEEE)") {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'Protovision') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'Unravel') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'SID') {
       this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
         }, error => {
          console.log(error);
        });
     } else if (this.PartForm.value.event === 'Matargashti') {
       this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
         }, error => {
          console.log(error);
        });
     } else if (this.PartForm.value.event === 'Deadshot') {
       this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
         }, error => {
          console.log(error);
        });
     } else if (this.PartForm.value.event === 'Pubg(DUO)') {
       this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
         }, error => {
          console.log(error);
        });
     }else if (this.PartForm.value.event === 'Pubg(SQUAD)') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'CSGO') {
       this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
         }, error => {
          console.log(error);
        });
     } else if (this.PartForm.value.event === 'Rugby') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'Knock It Down') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'Carrom') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'Arm Wrestling') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    } else if (this.PartForm.value.event === 'VR') {
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
       this.participants = success;
       console.log(success);
        }, error => {
         console.log(error);
       });
    }
  }
  edit(p) {
    const navigationExtras: NavigationExtras = {
      state: {
        user: p
      }
    };
    this.router.navigate(['editform'], navigationExtras);
  }
  share(p) {
    let navigationExtras: NavigationExtras = {
      state: {
        user: p.data
      }
    };
    this.router.navigate(['barcode'], navigationExtras);
  }

}
