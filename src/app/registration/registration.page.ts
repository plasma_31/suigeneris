import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastController, AlertController } from '@ionic/angular';
import {EventService} from '../event.service';
import { Router, PRIMARY_OUTLET } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  registerForm = new FormGroup({
    uname: new FormControl('', Validators.required),
    pass: new FormControl('', Validators.required),
    conpass: new FormControl('', Validators.required),
    post: new FormControl('', Validators.required),
    key: new FormControl('', Validators.required)
  });

  constructor(
      private alertCtrl: AlertController,
      private toastCtrl: ToastController,
      private eveSer: EventService,
      private router: Router
    ) { }

  ngOnInit() {

  }
  register() {
    const key = 'n3drpXern';
    if (this.registerForm.value.pass === this.registerForm.value.conpass) {
      if (this.registerForm.value.key === key) {
        const user: any = {};
        user.name = this.registerForm.value.uname;
        user.password = this.registerForm.value.pass;
        user.post = this.registerForm.value.post;
        this.eveSer.adduser(user);
        this.toastCtrl.create({
            message: 'Registered Successfully',
            duration: 2000,
            color: 'medium',
            position: 'bottom'
          }).then(toast => {
            toast.present();
          });
        this.registerForm.reset();
        this.router.navigateByUrl('home');
      } else {
        this.alertCtrl.create({
          header: 'Error',
          message: 'Key does not match',
          buttons: [
              {
                  text: 'Ok',
                  role: 'cancel'
              }
          ]
      }).then(alertEl => {
          alertEl.present();
      });
      }
    } else {
      this.alertCtrl.create({
        header: 'Error',
        message: 'Passwords do not match',
        buttons: [
            {
                text: 'Ok',
                role: 'cancel'
            }
        ]
    }).then(alertEl => {
        alertEl.present();
    });
    }
    }
}
