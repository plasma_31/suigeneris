
import { AlertController } from '@ionic/angular';
import { EventService } from './../event.service';
import { Component, OnInit } from '@angular/core';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-scan',
  templateUrl: './scan.page.html',
  styleUrls: ['./scan.page.scss'],
})
export class ScanPage implements OnInit {
  id: any;
  PartForm = new FormGroup({
    event: new FormControl('', Validators.required)
  });
  users: any;
  participants: any;
  currparticipant: any;
  check: any=null;
  attended: any;
  constructor(
    private bs: BarcodeScanner,
    private eveSer: EventService,
    private alertCtrl: AlertController) {
  }

  ngOnInit() {
    
  }
  getEntries() {
    this.bs.scan().then(barcodeData => {
      this.id = barcodeData.text;
      console.log(barcodeData.text);
    }).catch(err => {
         console.log('Error', err);
     });
    if (this.PartForm.value.event === "D'Bugging(IEEE)") {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === "D'Bugging(NON-IEEE)") {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'Protovision') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'Unravel') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'SID') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'Matargashti') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'Deadshot') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'Pubg(DUO)') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'Pubg(SQUAD)') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    } else if (this.PartForm.value.event === 'VR') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
    }else if (this.PartForm.value.event === 'CSGO') {
      this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
        this.attended = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
        this.participants = success;
        console.log(success);
        }, error => {
          console.log(error);
        });
      } else if (this.PartForm.value.event === 'Rugby') {
        this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
          this.attended = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
        this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
          this.participants = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
      } else if (this.PartForm.value.event === 'Knock It Down') {
        this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
          this.attended = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
        this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
          this.participants = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
      } else if (this.PartForm.value.event === 'Carrom') {
        this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
          this.attended = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
        this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
          this.participants = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
      } else if (this.PartForm.value.event === 'Arm Wrestling') {
        this.eveSer.getAttended('Attended '+this.PartForm.value.event).subscribe(success => {
          this.attended = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
        this.eveSer.getEvent(this.PartForm.value.event).subscribe(success => {
          this.participants = success;
          console.log(success);
          }, error => {
            console.log(error);
          });
      }
  }
  
  add() {
    const obj: any = {};
    obj.check = true;
    obj.data = this.currparticipant.data;
    this.eveSer.scan(this.currparticipant.data.event,this.currparticipant.id, obj);
    this.alertCtrl.create({

          header: 'Success',
          message: 'Marked As Attended',
          buttons: [
              {
                  text: 'Ok',
                  role: 'cancel'
              }
          ]
      }).then(alertEl => {
          alertEl.present();
      });
      this.PartForm.reset();
      this.id=null;
      this.currparticipant=null;
    
  }
  show() {
    let i, j;
    for ( i = 0; i < this.participants.length; i++) {
      if (this.id === this.participants[i].id) {
        for ( j = 0; j <= this.attended.length; j++) {
          console.log(this.participants[i].id);
          console.log('1');
          if (this.participants[i].id === this.attended[j].id) {
            this.check = 'scanned';
            break;
          } else {
            this.check = 'assign';
            console.log('1');
            this.currparticipant = this.participants[i];
            console.log(this.currparticipant);
          }
        }
        console.log(this.check);
        // if(this.check==='assign'){
        //   this.currparticipant=this.participants[i];
        //     console.log(this.currparticipant);
        //     this.check=null;
        //     break;
        // }else{
        if (this.check === 'scanned') {
          break;
        }
      } else {
        this.check = 'failed';
      }
    }
    console.log(this.check);
    if (this.check === 'scanned') {
      this.alertCtrl.create({
        header: 'Error',
        message: 'QRCode already scanned',
        buttons: [
            {
                text: 'Ok',
                role: 'cancel'
            }
        ]
    }).then(alertEl => {
        alertEl.present();
    });
    this.id=null;
    } else if (this.check === 'failed') {
        this.alertCtrl.create({

          header: 'Error',
          message: 'You are not registered OR searching in wrong event',
          buttons: [
              {
                  text: 'Ok',
                  role: 'cancel'
              }
          ]
      }).then(alertEl => {
          alertEl.present();
      });
        this.PartForm.reset();
        this.id=null;
  }
  }
}
