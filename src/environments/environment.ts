// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCY7pMmolq71sF5uSdZtCrXdSuRe4j9L2M",
    authDomain: "suigeneris-91e90.firebaseapp.com",
    databaseURL: "https://suigeneris-91e90.firebaseio.com",
    projectId: "suigeneris-91e90",
    storageBucket: "suigeneris-91e90.appspot.com",
    messagingSenderId: "942349921706",
    appId: "1:942349921706:web:897991dea6b3a0f3"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
